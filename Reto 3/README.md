# Reto 3

Este preto consiste en ejecutar pipeline en gitlab, para ello realizar lo siguiente:

- Crear un repositorio o rama en gitlab.com y copiar el contenido de esta carpeta al repositorio o rama.

- En caso de haber creado una rama, se requiere proteger la misma, esto se realica accediendo a la consiguración del repositorio, sección repositorio, acción proteger rama, indicar perfiles que puedan ejecutar el proceso CI/CD.

- Ejecutar CI, la ejecución es automatica en caso de haber dejado el contenido de esta carpeta en la rama main o master del proyecto, igualmente, si se crea una rama para ejecutar CI, luego de proteger dicha rama, se ejecuta automaticamente el proceso, en caso de ejecutar manualmente el proceso, acceder al menú CI/CD del repositorio, hacer clic en botón run pipeline, seleccionar rama y clic en botón run pipeline.

- Para visualizar la ejecución del pipeline, acceder a cada uno de los stages que se muestran durante la ejecución.
